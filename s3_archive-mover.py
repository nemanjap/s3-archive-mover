import json
import paramiko
import os
import subprocess


def create_ssh_client(ip, port, user, password):
    """

    Creates an SSH connection to the specified server

    :param ip: Servers' IP address or an URL
    :type ip: str
    :param port: Server port on which SSH is listening
    :type port: int
    :param user: Username used for the SSH connection
    :type user: str
    :param password: User password for the SSH connection
    :type password: str
    :returns: SSH client object
    :rtype: paramiko.SSHClient
    """
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, port, user, password)
    return client


def get_server_info(location):
    """

    Gathers all of the necessary information about the DB server based on the location information.
    Information is located in 'properties.json' configuration file and servers are classified based on their location.
    As we have one main DB server per DC location on which the archives are located this classification made sense.

    :param location: DC location of the targeted server
    :type location: str
    :returns: Dictionary with all necessary server info
    :rtype: dict
    """
    with open('properties.json') as data_file:
        data = json.load(data_file)
        return data[location]


def parse_listing(listing, directory):
    """

    Creates a dictionary describing the archive based on the provided listing.
    Provided listing is a list containing output form a 'du' Linux command ran against the give archive.
    Created dict contains number of archives that have the relative path of the archive as the key,
    and absolute path and archive size as the value

    :param listing: List containing 'ls -l' output for the given folder
    :type listing: list
    :param directory: Directory location of the archive
    :type directory: str
    :returns: Dictionary of archives with rel_pat as keys, and abs_path and size as values
    :rtype: dict
    """
    listing_dict = {}
    for item in listing:
        split_item = item.split('\t')
        listing_dict[os.path.relpath(split_item[1], directory)] = [split_item[1], split_item[0]]
    return listing_dict


def get_remote_listing(ip, port, user, password, remote_dir):
    """

    Lists the contents of the archive folder on the remote server.

    :param ip: Servers' IP address or an URL
    :type ip: str
    :param port: Server port on which SSH is listening
    :type port: int
    :param user: Username used for the SSH connection
    :type user: str
    :param password: User password for the SSH connection
    :type password: str
    :param remote_dir: Path to the directory holding the archives on the remote server
    :type remote_dir: str
    :returns: Dictionary containing information about the listing on the remote server
    :rtype: dict
    """
    ssh = create_ssh_client(ip, port, user, password)
    try:
        stdin, stdout, stderr = ssh.exec_command('du '+remote_dir+'*')
        listing = [line.strip('\n') for line in stdout.readlines()]
        return parse_listing(listing, remote_dir)
    finally:
        ssh.close()


def get_s3_listing(s3_path):
    """

    Lists the contents of the s3 bucket

    :param s3_path: AWS S3 path for the bucket which content is to be listed
    :type s3_path: str
    :returns: S3 bucket listing
    :rtype: list
    :raises: subprocess.CalledProcessError if there is an error in executing the aws command on the local server
    """
    try:
        listing = subprocess.check_output(['aws', 's3', 'ls', s3_path]).decode(encoding="utf-8").strip('\n')
        listing = [item.strip(' ').strip('PRE').strip(' ').rstrip('/') for item in listing.split('\n') if 'PRE' in item]
        return listing
    except subprocess.CalledProcessError as err:
        raise err


def get_data_for_transfer(remote_listing, s3_listing):
    """

    Calculates the differences between the content located on remote server and the one located on AWS S3.
    Those differences determine which files are to be fetched from the remote DB server and uploaded to S3.

    :param remote_listing: Listing from the remote DB server
    :type remote_listing: dict
    :param s3_listing: Listing from the AWS S3 bucket
    :type s3_listing: list
    :returns: A dict of content differences between remote DB server and S3
    :rtype: dict
    """
    data_for_transfer = {}
    listing_difference = set(remote_listing.keys()) - set(s3_listing)
    for key, value in remote_listing.items():
        if key in listing_difference:
            data_for_transfer[key] = value
    return data_for_transfer


def sftp_archives(ip, port, user, password, local_dir, data_for_transfer):
    ssh = create_ssh_client(ip, port, user, password)
    sftp = ssh.open_sftp()
    try:
        for key, value in data_for_transfer.items():
            local_path = os.path.join(local_dir, key)
            sftp.get(value[0], local_path)
    finally:
        sftp.close()
        ssh.close()


if __name__ == '__main__':
    sov_info = get_server_info('')
    ip = sov_info['ip']
    port = int(sov_info['port'])
    user = sov_info['user']
    password = sov_info['password']
    remote_dir = sov_info['remote_dir']
    local_dir = sov_info['local_dir']

    remote_listing = get_remote_listing(ip, port, user, password, remote_dir)
    s3_listing = get_s3_listing('')

    data_for_transfer = get_data_for_transfer(remote_listing, s3_listing)
    sftp_archives(ip, port, user, password, local_dir, data_for_transfer)
